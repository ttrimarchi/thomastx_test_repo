#pragma once

#ifdef BUILD_DLL
#define IN_OR_EX_PORT __declspec(dllexport)
#else
#define IN_OR_EX_PORT __declspec(dllimport)
#endif
namespace Unmanaged
{
	//
	// Export the entire class when we build the dll, and
	// Import it when we consume it.
	class IN_OR_EX_PORT NativeCPP
	{
	private:
		NativeCPP(){}
	public:
		NativeCPP(int);
		int value;
		int arraySize;
		int* _array;
		int Test(int i);
	};
}
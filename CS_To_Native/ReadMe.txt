This is a simple demo of mixing a C# GUI, managed C++, and native C++ in the same solution.

An Unmanaged dll contains a field , a function, and an array.

A managed dll wraps the Unmanaged DLL.

Everything is exposed to C#. The entire dll is exported with a single statement.

A property is used such that accessing a native field has the normal syntax. Note, that the set and get are inline. It�s not too hard to imagine what the compiler will do when it optimizes. It should get totally optimized away.

Memcpy is used to demonstrate a direct mapping between a C# array and a native array.

The function is:
int Test(int);
which demonstrates both passing and receiving values.

Build it in Debug mode if you want to step through it.

Build it in release mode for timing purposes.

﻿namespace CS_To_Native
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonDoSomething = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBoxNativeArraySize = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.textBoxTransferTime = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.textBoxNativeValue = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.textBoxShouldBe = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.textBoxFunctionArgument = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.textBoxFunctionResult = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonDoSomething
            // 
            this.buttonDoSomething.Location = new System.Drawing.Point(12, 31);
            this.buttonDoSomething.Name = "buttonDoSomething";
            this.buttonDoSomething.Size = new System.Drawing.Size(88, 31);
            this.buttonDoSomething.TabIndex = 0;
            this.buttonDoSomething.Text = "Do Stuff";
            this.buttonDoSomething.UseVisualStyleBackColor = true;
            this.buttonDoSomething.Click += new System.EventHandler(this.buttonDoSomething_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textBoxNativeArraySize);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(115, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(104, 58);
            this.panel1.TabIndex = 1;
            // 
            // textBoxNativeArraySize
            // 
            this.textBoxNativeArraySize.Location = new System.Drawing.Point(6, 25);
            this.textBoxNativeArraySize.Name = "textBoxNativeArraySize";
            this.textBoxNativeArraySize.Size = new System.Drawing.Size(85, 20);
            this.textBoxNativeArraySize.TabIndex = 1;
            this.textBoxNativeArraySize.Text = "1000";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Native Array Size";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.textBoxTransferTime);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(225, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(104, 58);
            this.panel2.TabIndex = 2;
            // 
            // textBoxTransferTime
            // 
            this.textBoxTransferTime.Location = new System.Drawing.Point(6, 25);
            this.textBoxTransferTime.Name = "textBoxTransferTime";
            this.textBoxTransferTime.Size = new System.Drawing.Size(85, 20);
            this.textBoxTransferTime.TabIndex = 1;
            this.textBoxTransferTime.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Transfer Time us";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.textBoxNativeValue);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Location = new System.Drawing.Point(225, 76);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(104, 58);
            this.panel3.TabIndex = 3;
            // 
            // textBoxNativeValue
            // 
            this.textBoxNativeValue.Location = new System.Drawing.Point(6, 25);
            this.textBoxNativeValue.Name = "textBoxNativeValue";
            this.textBoxNativeValue.Size = new System.Drawing.Size(85, 20);
            this.textBoxNativeValue.TabIndex = 1;
            this.textBoxNativeValue.Text = "2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Native value";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.textBoxShouldBe);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Location = new System.Drawing.Point(225, 164);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(104, 58);
            this.panel4.TabIndex = 4;
            // 
            // textBoxShouldBe
            // 
            this.textBoxShouldBe.Location = new System.Drawing.Point(6, 25);
            this.textBoxShouldBe.Name = "textBoxShouldBe";
            this.textBoxShouldBe.Size = new System.Drawing.Size(85, 20);
            this.textBoxShouldBe.TabIndex = 1;
            this.textBoxShouldBe.Text = "1000";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Function should be";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.textBoxFunctionArgument);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Location = new System.Drawing.Point(115, 76);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(104, 58);
            this.panel5.TabIndex = 5;
            // 
            // textBoxFunctionArgument
            // 
            this.textBoxFunctionArgument.Location = new System.Drawing.Point(6, 25);
            this.textBoxFunctionArgument.Name = "textBoxFunctionArgument";
            this.textBoxFunctionArgument.Size = new System.Drawing.Size(85, 20);
            this.textBoxFunctionArgument.TabIndex = 1;
            this.textBoxFunctionArgument.Text = "3";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Function Argument";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.textBoxFunctionResult);
            this.panel6.Controls.Add(this.label6);
            this.panel6.Location = new System.Drawing.Point(110, 164);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(104, 58);
            this.panel6.TabIndex = 6;
            // 
            // textBoxFunctionResult
            // 
            this.textBoxFunctionResult.Location = new System.Drawing.Point(6, 25);
            this.textBoxFunctionResult.Name = "textBoxFunctionResult";
            this.textBoxFunctionResult.Size = new System.Drawing.Size(85, 20);
            this.textBoxFunctionResult.TabIndex = 1;
            this.textBoxFunctionResult.Text = "1000";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Function Result";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(141, 148);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(154, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Function = value + ++argument";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(353, 242);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.buttonDoSomething);
            this.Name = "Form1";
            this.Text = "Demo - C# to/from Native C++";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonDoSomething;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxNativeArraySize;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox textBoxTransferTime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox textBoxNativeValue;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox textBoxShouldBe;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox textBoxFunctionArgument;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TextBox textBoxFunctionResult;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Managed;


namespace CS_To_Native
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        int arraySize;
        public Wrapper wrapper;
        public int[] data;
        private void buttonDoSomething_Click(object sender, EventArgs e)
        {
            //
            // Set the array sizes.
            arraySize = int.Parse(textBoxNativeArraySize.Text);
            wrapper = new Wrapper(arraySize);
            data = new int[arraySize];
            //
            // Call the function.
            int result;
            int nativeValue = int.Parse(textBoxNativeValue.Text);
            wrapper.value = nativeValue;
            int functionArgument = int.Parse(textBoxFunctionArgument.Text);
            result = wrapper.Test(functionArgument);
            textBoxFunctionResult.Text = result.ToString();
            textBoxShouldBe.Text = (nativeValue + ++functionArgument).ToString();
            //
            // Fill the array with data.
            for (int i = 0; i < data.Length; ++i)
            {
                data[i] = i;
            }
            int[] returnedData = new int[arraySize];
            int[] returndata2;
            //
            // Time transfering data between a C# array and a native array.
            DateTime start = DateTime.Now;
            int numLoops = 10000;
            int index = 0;
            for (int i = 0; i < numLoops; ++i)
            {
                // Do not allow things to get optimized away.
                if (++index >= data.Length) index = 0;
                data[index] = index;
                wrapper.PutArray(data);
                returndata2 = wrapper.GetArray(returnedData);
            }
            DateTime end = DateTime.Now;
            TimeSpan ts = end - start;
            double ms = ts.Milliseconds;
            double transferTime = (1000.0 * ms / numLoops) /2;
            textBoxTransferTime.Text = transferTime.ToString("F2");
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

    }
}

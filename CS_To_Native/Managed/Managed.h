// Managed.h

#pragma once
#include "Unmanaged\Unmanaged.h"
using namespace System;

namespace Managed 
{
	//
	// This class wraps the unmanaged dll and exposes all of it.
	// Note - inline everything for speed.
	public ref class Wrapper
	{
		// TODO: Add your methods for this class here.
	private:
		int arraySize;
		Wrapper(){}
		Unmanaged::NativeCPP* nativeCPP;
		array<int>^ _array;
	public:
		Wrapper(int size)
		{
			arraySize = size;
			//
			// make an unmanaged object
			nativeCPP = new Unmanaged::NativeCPP(arraySize);
			//
			// make an array the same size as the unmanaged one.
			_array = gcnew array<int>(arraySize);
		}
		//
		// make a property that behaives exactly like the unmamaged field.
		// If we use this make this into a macro.
		property int value
		{
			inline int get()
			{
				return nativeCPP->value;
			}
			inline void set(int arg)
			{
				nativeCPP->value = arg;
			}
		}
		int Test(int i);
		inline array<int>^ GetArray(array<int>^ arg);
		inline array<int>^ PutArray(array<int>^ arg);
	};
}

// This is the main DLL file.

#include "stdafx.h"
#include <string.h>
#include "Managed.h"

namespace Managed
{
	//
	// Prove that we can call native functions, pass arguments, and receive returned values.
	int Wrapper::Test(int i)
	{
		return nativeCPP->Test(i);
	}
	//
	// Prove that we can copy from a native array directly into a c# array.
	array<int>^  Wrapper::GetArray(array<int>^ arg)
	{
		pin_ptr<int> argStart = &arg[0];
		memcpy(argStart, nativeCPP->_array, nativeCPP->arraySize);
		return arg;
	}
	//
	// Prove that we can copy from a c# array directly into native array.
	array<int>^ Wrapper::PutArray(array<int>^ arg)
	{
		pin_ptr<int> argStart = &arg[0];
		memcpy(nativeCPP->_array, argStart, nativeCPP->arraySize);
		return arg;
	}
}

